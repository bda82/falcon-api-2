from notes.models import Note
from services import ServiceBase


class NoteService(ServiceBase):
    model = Note
